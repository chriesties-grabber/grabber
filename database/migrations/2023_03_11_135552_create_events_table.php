<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->integer('event_id')
                ->nullable(false)
                ->index();
            $table->string('landing_url')
                ->nullable(false);
            $table->string('title')
                ->collation('NOCASE')
                ->nullable(false)
                ->index();
            $table->string('subtitle')
                ->collation('NOCASE')
                ->nullable();
            $table->timestampTz('performed_at')
                ->nullable();
            $table->string('analytics_id')
                ->nullable(false)
                ->index();
            $table->string('sale_total_txt')
                ->nullable();
            $table->string('sale_total_val')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('events');
    }
};
