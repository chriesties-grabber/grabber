<?php

use App\Models\Event;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('event_items', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Event::class);
            $table->string('object_id');
            $table->string('url')->nullable();
            $table->string('title')->nullable(false)->index();
            $table->string('subtitle')->nullable();
            $table->string('description')->nullable();
            $table->string('price_estimated_low')->nullable(true);
            $table->string('price_estimated_high')->nullable(true);
            $table->string('price_estimated')->nullable(true);
            $table->string('price_realised')->nullable(true);
            $table->string('price_realised_txt')->nullable(true);
            $table->boolean('lot_withdrawn');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('event_items');
    }
};
