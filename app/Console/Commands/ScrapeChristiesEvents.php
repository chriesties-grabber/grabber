<?php

namespace App\Console\Commands;

use App\Models\Event;
use Illuminate\Console\Command;

class ScrapeChristiesEvents extends Command
{
    // TODO Toto se musí předělat... měsíce vyhodit, ty jsou jasný, 
    //      a u roků asi dát počáteční do konfiguace a pak už to bude
    //      celkem jasný...
    protected const MONTHS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    protected const YEARS = [
        '1998',
        '1999',
        '2000',
        '2001',
        '2002',
        '2003',
        '2004',
        '2005',
        '2006',
        '2007',
        '2008',
        '2009',
        '2010',
        '2011',
        '2012',
        '2013',
        '2014',
        '2015',
        '2016',
        '2017',
        '2018',
        '2019',
        '2020',
        '2021',
        '2022',
        '2023',
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:christies-events
        {year='.self::YEARS[0].' : Požadovaný rok}
        {month='.self::MONTHS[0].'  : Požadovaný měsíc}
        {--A|all : Stáhne všechna data}
        {--F|from= : Stáhne data od uvedeného roku a měsíce (např. 2023-1)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Stáhne data z Christies';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $year = $this->argument('year');
        $month = $this->argument('month');
        $grabAll = $this->option('all');
        $grabFromDate = $this->option('from', null); // TODO ...

        if ($grabAll) {
            collect(static::YEARS)
                ->each(function (string $year) {
                    collect(static::MONTHS)
                        ->each(function (string $month) use ($year) {
                            $this->processEventsRequest($this->prepareEventRequestUrl($year, $month));
                        });
                });
        } else {
            $this->processEventsRequest($this->prepareEventRequestUrl($year, $month));
        }
    }

    protected function prepareEventRequestUrl(string $year, string $month): string
    {
        return "https://www.christies.com/en/results?year={$year}&month={$month}";
    }

    protected function processEventsRequest(string $url): void
    {
        $t = file_get_contents($url);
        $ta = explode('<script>', $t);
        $tt = explode('</script>', $ta[10]);
        $tx = str_replace('window.chrComponents = window.chrComponents || {};', '', $tt[0]);
        $tx = str_replace('window.chrComponents.calendar = ', '', $tx);
        $tx = rtrim(trim($tx), ';');

        $i = strpos($tx, '"events":');
        $ty = str_replace('"events":', '', substr($tx, $i, -1));

        $i = strpos($ty, ' labels: {');
        $tz = rtrim(trim(substr($ty, 0, $i)), '},');

        collect(json_decode($tz))
            ->each(fn (object $event) => Event::create([
                'event_id' => $event->event_id,
                'landing_url' => $event->landing_url,
                'title' => $event->title_txt,
                'subtitle' => $event->subtitle_txt,
                'performed_at' => $event->start_date,
                'analytics_id' => $event->analytics_id,
                'sale_total_txt' => $event->sale_total_txt,
                'sale_total_val' => $event->sale_total_value_txt,
            ]));
    }
}
