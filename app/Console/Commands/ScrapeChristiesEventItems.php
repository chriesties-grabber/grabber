<?php

namespace App\Console\Commands;

use App\Models\Event;
use Illuminate\Console\Command;

class ScrapeChristiesEventItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:christies-event-items';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Stáhne detail první další nezprocesované aukce';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        Event::query()
            ->doesntHave('items')
            ->each(function (Event $event) {
                $eventUrl = $this->getEventDetailUrl($event);

                $t = file_get_contents($eventUrl);

                $i = strpos($t, 'window.chrComponents.lots = ');
                $t = str_replace('window.chrComponents.lots = ', '', substr($t, $i, -1));

                $i = strpos($t, '</script>');
                $t = substr($t, 0, $i);

                $t = rtrim(trim($t), ';');

                $data = json_decode($t);
                $lots = collect($data?->data?->lots)
                    ->map(fn (object $item) => [
                        'object_id' => $item->object_id,
                        'url' => $item->url,
                        'title' => $item->title_primary_txt,
                        'subtitle' => $item->title_secondary_txt,
                        'description' => $item->description_txt,
                        'price_estimated_low' => $item->estimate_low,
                        'price_estimated_high' => $item->estimate_high,
                        'price_estimated' => $item->estimate_txt,
                        'price_realised' => $item->price_realised,
                        'price_realised_txt' => $item->price_realised_txt,
                        'lot_withdrawn' => $item->lot_withdrawn,
                    ]);

                $event->items()->createMany($lots->toArray());
            });
    }

    protected function getEventDetailUrl(Event $event): string
    {
        return "{$event->landing_url}?loadall=true&page=2&sortby=lotnumber";
    }
}
