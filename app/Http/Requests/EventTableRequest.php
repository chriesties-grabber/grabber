<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventTableRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'filter' => [
                'present',
                'string',
                'nullable',
                'min:3',
            ],
        ];
    }
}
