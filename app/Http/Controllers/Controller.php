<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\View\View;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function indexAction(Request $request): View
    {
        return view('event-table', [
            'header' => 'Christie\'s - Auctions',
        ]);
    }

    public function eventItemsAction(Event $event, Request $request): View
    {
        return view('event-item-table', [
            'event' => $event,
            'header' => 'Christie\'s - Auction items',
        ]);
    }
}
