<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

/**
 * @todo Tady máme zatím problém, že po vyvolání hledání
 *       se pak refreshne tabulka, ale ne hlavička...
 */
class TableHeaderColumn extends Component
{
    /**
     * @var string $column
     */
    public string $column;

    /**
     * @var string $title
     */
    public string $title;

    /**
     * @var string|null $dir
     */
    public string|null $dir = null;

    protected $listeners = ['refreshTableHeader' => '$refresh'];

    public function render(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        Log::info("[TableHeaderColumn]::render('{$this->column}', '{$this->title}', '{$this->dir}')");

        return view('livewire.table-header-column', [
            'column' => $this->column,
            'title' => $this->title,
            'dir' => $this->dir,
        ]);
    }

    /**
     * @param string $column
     * @param string|null $dir
     * @return void
     * @throws \Exception
     */
    public function sortBy(string $column, string|null $dir): void
    {
        Log::info("[TableHeaderColumn]:'{$column}', '{$dir}'");

        $this->emitTo('event-table', 'sortBy', $column, $dir);
        //$this->emitUp('sortBy', $column,  $dir);
    }
}
