<?php

namespace App\Http\Livewire;

use App\Models\Event;
use App\Models\EventItem;
use Illuminate\View\View;
use Livewire\Component;

class EventItemTable extends Component
{
    public Event $event;

    /**
     * @param Event $event
     * @return void
     */
    public function mount(Event $event): void
    {
        $this->event = $event;
    }

    /**
     * @return View
     */
    public function render(): View
    {
        return view('livewire.event-item-table', [
            'event' => $this->event,
            'eventItems' => EventItem::with(['event'])
                ->where('event_id', '=', $this->event->id)
                ->paginate(25),
        ]);
    }
}
