<?php

namespace App\Http\Livewire;

use App\Http\Requests\EventTableRequest;
use App\Models\Event;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Livewire\Component;

/**
 * @todo Ve výsledku by měl býl filtr samostatná komponenta a umožňovat
 *       skládání filtrů (jako např. v Gitlabu u issues)...
 * @todo {@see EventTableFilter}
 */
class EventTable extends Component
{
    /**
     * Current table filter.
     *
     * @var string $filter
     */
    public string $filter = '';

    protected $listeners = [
        'sortBy',
    ];

    /**
     * Contain array of columns name with sort directiln.
     *
     * @var array|string[] $sortBy
     */
    private array $sortBy = [
        'event_id' => null,
        'title' => null,
        'performed_at' => null,
        'currency' => null,
        'sale_total' => null,
        'items_count' => null,
    ];

    /**
     * @var EventTableRequest|null $request
     */
    private EventTableRequest|null $request = null;

    /**
     * @return View
     */
    public function render(): View
    {
        Log::info("[EventTable]:render()");

        $this->dispatchBrowserEvent('table-updated');

        return view('livewire.event-table', [
            'events' => $this->events(),
            'request' => $this->request,
            'sortBy' => $this->sortBy,
        ]);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        if (!($this->request instanceof EventTableRequest)) {
            $this->request = new EventTableRequest();
        }

        return $this->request->rules();
    }

    /**
     * @return void
     */
    public function submit(): void
    {
        $this->filter = $this->validate()['filter'];
    }


    /**
     * @throws ValidationException
     */
    public function updated($propertyName = 'filter'): void
    {
        $this->filter = $this->validateOnly($propertyName)['filter'];
    }

    /**
     * @param string $column
     * @param string|null $dir
     * @return void
     * @throws \Exception
     */
    public function sortBy(string $column, string|null $dir): void
    {
        Log::info("[EventTable]:'{$column}', '{$dir}'");

        if (!array_key_exists($column, Event::FILTERABLE)) {
            throw new \Exception("Bad sort column name given (\"{$column}\")!");
        }

        if (!empty($dir) && !in_array(strtolower($dir), ['asc', 'desc', ''])) {
            throw new \Exception("Bad sort direction given (\"{$dir}\")!");
        }

        $this->sortBy[$column] = match ($dir) {
            'asc' => 'desc',
            'desc' => null,
            default => 'asc',
        };
    }

    /**
     * @return LengthAwarePaginator
     */
    private function events(): LengthAwarePaginator
    {
        $builder = Event::query()
            ->select('events.*')
            ->selectRaw('trim(substr(events.sale_total_val, instr(events.sale_total_val, \' \') - 3, 3)) AS currency')
            ->selectRaw('replace(trim(substr(events.sale_total_val, instr(events.sale_total_val, \' \'))), \',\', \'\') AS sale_total')
            ->selectRaw('COUNT(event_items.id) AS items_count')
            ->leftJoin('event_items', 'event_items.event_id', '=', 'events.id')
            ->groupBy('events.event_id');

        if (!empty($this->filter)) {
            $dbConnection = config('database.default');
            $dbDriver = config("database.connections.{$dbConnection}.driver");

            if ($dbDriver === 'sqlite') {
                $builder->whereRaw('events.title LIKE "%' . htmlspecialchars($this->filter) . '%"');
            } else {
                $builder->whereFullText('events.title', htmlspecialchars($this->filter));
            }
        }

        collect($this->sortBy)
            ->each(fn (string|null $dir, string $col) =>
                is_null($dir) || $builder->orderBy(Event::FILTERABLE[$col], "{$dir}")
            );

        return $builder->paginate(25);
    }
}
