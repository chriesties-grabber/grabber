<?php

namespace App\Http\Livewire;

use Livewire\Component;

class EventTableFilter extends Component
{
    public function render()
    {
        return view('livewire.event-table-filter');
    }
}
