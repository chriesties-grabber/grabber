<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer $id
 * @property integer $event_id
 * @property string $object_id
 * @property string $url
 * @property string $title
 * @property string $subtitle
 * @property string $description
 * @property string $price_estimated_low
 * @property string $price_estimated_high
 * @property string $price_realised
 * @property string $lot_withdrawn
 * @property-read Event $event
 */
class EventItem extends Model
{
    use HasFactory;

    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $fillable = [
        'event_id',
        'object_id',
        'url',
        'title',
        'subtitle',
        'description',
        'price_estimated_low',
        'price_estimated_high',
        'price_realised',
        'lot_withdrawn',
    ];

    /**
     * @return BelongsTo
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }
}
