<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * @property integer $id
 * @property integer $event_id
 * @property string $landing_url
 * @property string $title
 * @property string $subtitle
 * @property Carbon $performed_at
 * @property string $analytics_id
 * @property string $sale_total_txt
 * @property string $sale_total_val
 * @property-read Collection<EventItem> $items
 */
class Event extends Model
{
    use HasFactory;

    public const CREATED_AT = null;
    public const UPDATED_AT = null;

    /**
     * Array of filterable columns.
     */
    public const FILTERABLE = [
        'event_id' => 'events.event_id',
        'title' => 'events.title',
        'performed_at' => 'events.performed_at',
        'currency' => 'currency',
        'sale_total' => 'sale_total',
        'items_count' => 'items_count',
    ];

    /**
     * @var string
     */
    public $table ='events';

    protected $fillable = [
        'event_id',
        'landing_url',
        'title',
        'subtitle',
        'performed_at',
        'analytics_id',
        'sale_total_txt',
        'sale_total_val',
    ];

    protected $casts = [
        'performed_at' => 'datetime:Y-m-d',
    ];

    /**
     * @return HasMany
     */
    public function items(): HasMany
    {
        return $this->hasMany(EventItem::class);
    }
}
