<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\EventTableFilter;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class EventTableFilterTest extends TestCase
{
    /** @test */
    public function the_component_can_render()
    {
        $component = Livewire::test(EventTableFilter::class);

        $component->assertStatus(200);
    }
}
