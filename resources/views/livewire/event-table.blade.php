<div>
    <table class="table-auto">
        <thead>
            <tr>
                <td colspan="6">
                    <form class="w-full bg-white shadow-md rounded"
                          wire:submit.prevent="submit">
                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                               id="filter"
                               type="text"
                               placeholder="filter"
                               wire:model="filter"
                        >
                    </form>
                </td>
            </tr>
        </thead>
        <thead>
            <tr>
                <th class="text-left">
                    <!-- TODO Toto by taky ve výsledku měla být vlastní komponenta... -->
                    <a wire:click.prevent="sortBy('event_id', '{{ $sortBy['event_id'] }}')" role="button" href="#">
                        <span class="pr-2">Event ID</span>
                        @if ($sortBy['event_id'] === 'asc')
                            &#8593;
                        @elseif ($sortBy['event_id'] === 'desc')
                            &#8595;
                        @else
                            ~
                        @endif
                    </a>
                </th>
                <th class="text-left">
                    <a wire:click.prevent="sortBy('title', '{{ $sortBy['title'] }}')" role="button" href="#">
                        <span class="pr-2">Title</span>
                        @if ($sortBy['title'] === 'asc')
                            &#8593;
                        @elseif ($sortBy['title'] === 'desc')
                            &#8595;
                        @else
                            ~
                        @endif
                    </a>
                </th>
                <th class="text-right">
                    {{--
                    @livewire('table-header-column', ['column' => 'performed_at', 'title' => 'Performed At', 'dir' => $sortBy['performed_at']])

                    <livewire:table-header-column
                        column="performed_at"
                        title="Performed At"
                        wire:dir="$sortBy['performed_at']"
                        key="performed_at_col"
                    />
                    --}}
                    <a wire:click.prevent="sortBy('performed_at', '{{ $sortBy['performed_at'] }}')" role="button" href="#">
                        <span class="pr-2">Performed At</span>
                        @if ($sortBy['performed_at'] === 'asc')
                            &#8593;
                        @elseif ($sortBy['performed_at'] === 'desc')
                            &#8595;
                        @else
                            ~
                        @endif
                    </a>
                </th>
                <th class="text-right">
                    <a wire:click.prevent="sortBy('currency', '{{ $sortBy['currency'] }}')" role="button" href="#">
                        <span class="pr-2">Currency</span>
                        @if ($sortBy['currency'] === 'asc')
                            &#8593;
                        @elseif ($sortBy['currency'] === 'desc')
                            &#8595;
                        @else
                            ~
                        @endif
                    </a>
                </th>
                <th class="text-right">
                    <a wire:click.prevent="sortBy('sale_total', '{{ $sortBy['sale_total'] }}')" role="button" href="#">
                        <span class="pr-2">Sale Total</span>
                        @if ($sortBy['sale_total'] === 'asc')
                            &#8593;
                        @elseif ($sortBy['sale_total'] === 'desc')
                            &#8595;
                        @else
                            ~
                        @endif
                    </a>
                </th>
                <th class="text-right">
                    <a wire:click.prevent="sortBy('items_count', '{{ $sortBy['items_count'] }}')" role="button" href="#">
                        <span class="pr-2">Items count</span>
                        @if ($sortBy['items_count'] === 'asc')
                            &#8593;
                        @elseif ($sortBy['items_count'] === 'desc')
                            &#8595;
                        @else
                            ~
                        @endif
                    </a>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($events as $event)
                <tr>
                    <td>{{ $event->event_id }}</td>
                    <td>
                        @if (!empty($event->landing_url))
                           <a class="underline" href="{{ $event->landing_url }}" target="blank">
                               {{ $event->title }}
                           </a>
                        @else
                            {{ $event->title }}
                        @endif
                    </td>
                    <td class="text-right">{{ \Illuminate\Support\Carbon::make($event->performed_at)->format('j.n. Y') }}</td>
                    <td class="text-right">{{ $event->currency }}</td>
                    <td class="text-right">{{ $event->sale_total }}</td>
                    <td class="text-right">
                        <a class="underline" href="{{ route('eventItemsTable', ['event' => $event]) }}">{{ $event->items_count }}</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tbody>
            <tr>
                <td colspan="6">
                    {{ $events->links() }}
                </td>
            </tr>
        </tbody>
    </table>
</div>
