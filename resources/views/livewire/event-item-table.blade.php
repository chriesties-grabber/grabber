<div class="w-75 sm:p-6 xl:p-8">
    <table class="w-full">
        <thead>
        <tr>
            <th>Event ID</th>
            <th>Object ID</th>
            <th>Title</th>
            <th>Price estimated</th>
            <th>Price realised</th>
        </tr>
        </thead>
        <tbody>
            @foreach($eventItems as $eventItem)
                <tr>
                    <td>{{ $eventItem->event_id }}</td>
                    <td>{{ $eventItem->object_id }}</td>
                    <td>{{ $eventItem->title }}</td>
                    <td>{{ $eventItem->price_estimated }}</td>
                    <td>{{ $eventItem->price_realised }}</td>
                </tr>
            @endforeach
        </tbody>
        <tbody>
            <tr>
                <td colspan="5">
                    {{ $eventItems->links() }}
                </td>
            </tr>
        </tbody>
    </table>
</div>
