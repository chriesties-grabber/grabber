<a
    wire:click="sortBy('{{ $column }}', '{{ $dir }}')"
    role="button"
    href="#"
>
    <span class="pr-2">{{ $title }}</span>
    @if ($dir === 'asc')
        &#8593;
    @elseif ($dir === 'desc')
        &#8595;
    @else
        ~
    @endif
</a>
