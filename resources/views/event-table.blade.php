@extends('layouts.app')

@section('title', 'Christie\'s Auctions Data')

@section('content')
    <livewire:event-table/>
@endsection
