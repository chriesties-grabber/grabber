@extends('layouts.app')

@section('title', 'Christie\'s Auctions Data')

@section('content')
    <livewire:event-item-table :event="$event"/>
@endsection
