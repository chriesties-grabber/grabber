# Christie's Auctions Grabber

Simple [Laravel](https://laravel.com) application for grabbing data about auctions from __Christie's__ websites.

## Usage

We are using default configuration provided by __Laravel Sail__ so firstly do this:

```shell
docker compose up -d
docker compose exec laravel.test bash
```

For grabbing are there these commands:

```shell
# Download all auctions
php artisan app:christies-events -a

# Download auctions in given time period
php artisan app:christies-events --year=2023 --month=11

# Download all auction items for all auctions
php artisan app:christies-event-items
```

For displaying the data visit [localhost](http://localhost).

You should see something like this:

 ![Running application](/docs/img.png)
